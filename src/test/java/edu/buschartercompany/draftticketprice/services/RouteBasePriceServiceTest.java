package edu.buschartercompany.draftticketprice.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.buschartercompany.draftticketprice.exceptions.RouteInputException;

class RouteBasePriceServiceTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	final void testRouteBasePriceService() {
		try {
			RouteBasePriceService s = new RouteBasePriceService("LT_VILNIUS");
			assertEquals(s.getRoute().getBaseNettoPrice(), 10d);
			assertEquals(s.getRoute().getCurrencyCode(), "EUR");
			s = new RouteBasePriceService("EE_VALGA");
			assertEquals(s.getRoute().getBaseNettoPrice(), 5d);
			assertEquals(s.getRoute().getCurrencyCode(), "EUR");
		} catch (RouteInputException e) {
			fail(e.getMessage());
		}
	}

}
