package edu.buschartercompany.draftticketprice.services;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.buschartercompany.draftticketprice.entities.DraftPriceRequest;
import edu.buschartercompany.draftticketprice.entities.Luggage;
import edu.buschartercompany.draftticketprice.entities.Passenger;
import edu.buschartercompany.draftticketprice.entities.Route;
import edu.buschartercompany.draftticketprice.exceptions.IllegalAgeException;
import edu.buschartercompany.draftticketprice.exceptions.PersonCategoryInputException;
import edu.buschartercompany.draftticketprice.exceptions.RouteInputException;

class DraftTicketPriceServiceTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	final void testDraftTicketPrice() {
		DraftTicketPriceService dtps = new DraftTicketPriceService();
		DraftPriceRequest req = new DraftPriceRequest();
		Route r = new Route();
		List<Passenger> passengers = new ArrayList<Passenger>();
		Passenger p = new Passenger();
		Luggage lu = new Luggage();
		r.setRouteCode("LT_vilnius"); // should be case insensitive
		lu.setCnt(2);
		p.setForename("JĀNIS");
		p.setSurname("PROTIBĒRZIŅŠ");
		p.setBirthDate(LocalDate.of(1985, 9, 21));
		p.setLuggage(lu);
		passengers.add(p);
		p = new Passenger();
		lu = new Luggage();
		lu.setCnt(1);
		p.setForename("JĀNABĒRNS");
		p.setSurname("PROTIBĒRZIŅŠ");
		p.setBirthDate(LocalDate.of(2014, 1, 11));
		p.setLuggage(lu);
		passengers.add(p);
		req.setRoute(r);
		req.setPassengers(passengers);
		try {
			dtps.DraftTicketPrice(req);
		} catch (RouteInputException e) {
			fail(e.getMessage());
		} catch (IllegalAgeException e) {
			fail(e.getMessage());
		} catch (PersonCategoryInputException e) {
			fail(e.getMessage());
		}
		assertNotNull(dtps.getDraftTicketPrice().getRoute());
		assertEquals(dtps.getDraftTicketPrice().getRoute().getRouteCode(), "LT_VILNIUS");
		assertEquals(dtps.getDraftTicketPrice().getDraftPrice().getTotalPrice().getPriceBrutto(), 29.04);
		assertEquals(dtps.getDraftTicketPrice().getDraftPrice().getTotalPrice().getPriceNetto(), 24.00);
		assertEquals(dtps.getDraftTicketPrice().getDraftPrice().getTotalPrice().getTaxRate(), 21.00);
		assertEquals(dtps.getDraftTicketPrice().getDraftPrice().getTotalPrice().getCurrencyCode(), "EUR");
	}

}
