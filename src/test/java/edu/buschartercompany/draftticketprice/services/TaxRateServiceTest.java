package edu.buschartercompany.draftticketprice.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TaxRateServiceTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	final void testGetTaxRate() {
		TaxRateService trs = new TaxRateService();
		assertEquals(trs.getTaxRate().getTaxRate(), 21d, 0);
		assertEquals(trs.getTaxRate().getCurrencyCode(), "EUR");
	}

}
