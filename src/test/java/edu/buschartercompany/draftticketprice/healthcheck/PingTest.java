package edu.buschartercompany.draftticketprice.healthcheck;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PingTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	final void testIsServiceAvailable() {
		Ping p = new Ping();
		assertTrue(p.isServiceAvailable());
	}

}
