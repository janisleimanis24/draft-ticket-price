package edu.buschartercompany.draftticketprice.business;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.buschartercompany.draftticketprice.entities.Price;
import edu.buschartercompany.draftticketprice.exceptions.IllegalAgeException;
import edu.buschartercompany.draftticketprice.exceptions.RouteInputException;

class PriceCalculationRulesTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	final void testGetCategoryCodeByBirthDate() {
		PriceCalculationRules cr = new PriceCalculationRules();
		LocalDate birthDate;
		try {
			birthDate = LocalDate.of(1981, 9, 24);
			assertEquals(cr.getCategoryCodeByBirthDate(birthDate), "ADULT");
		} catch (IllegalAgeException e) {
			fail(e.getMessage());
		}
		try {
			birthDate = LocalDate.of(2004, 12, 24); // this test will fail in the future; ToDo: make it dynamic
			assertEquals(cr.getCategoryCodeByBirthDate(birthDate), "CHILD");
		} catch (IllegalAgeException e) {
			fail(e.getMessage());
		}
	}

	@Test
	final void testGetLuggagePrice() {
		PriceCalculationRules cr = new PriceCalculationRules();
		try {
			assertEquals(cr.getLuggagePrice("LT_VILNIUS"), 3.00);
		} catch (RouteInputException e) {
			fail(e.getMessage());
		}
	}

	@Test
	final void testGetPersonsPrice() {
		PriceCalculationRules cr = new PriceCalculationRules();
		try {
			assertEquals(cr.getPersonsPrice("LT_VILNIUS", "ADULT"), 10.00);
		} catch (RouteInputException e) {
			fail(e.getMessage());
		}
		try {
			assertEquals(cr.getPersonsPrice("LT_VILNIUS", "CHILD"), 5.00);
		} catch (RouteInputException e) {
			fail(e.getMessage());
		}
	}

	@Test
	final void testGetPriceWithTaxes() {
		PriceCalculationRules cr = new PriceCalculationRules();
		Price p = cr.getPriceWithTaxes(100);
		assertEquals(p.getPriceNetto(), 100);
		assertEquals(p.getTaxRate(), 21);
		assertEquals(p.getPriceBrutto(), 121);
		assertEquals(p.getCurrencyCode(), "EUR");
	}

}
