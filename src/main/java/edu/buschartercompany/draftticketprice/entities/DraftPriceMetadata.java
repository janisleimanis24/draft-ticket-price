package edu.buschartercompany.draftticketprice.entities;

import java.time.LocalDateTime;
import java.util.Random;

public class DraftPriceMetadata {

	private long calculationId;
	private LocalDateTime calculationDateTime;

	public DraftPriceMetadata() {
		this.calculationId = Math.abs(new Random().nextLong());
		this.calculationDateTime = LocalDateTime.now();
	}

	public long getCalculationId() {
		return calculationId;
	}

	public LocalDateTime getCalculationDateTime() {
		return calculationDateTime;
	}

}
