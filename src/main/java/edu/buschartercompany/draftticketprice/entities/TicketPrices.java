package edu.buschartercompany.draftticketprice.entities;

import java.util.ArrayList;
import java.util.List;

public class TicketPrices {

	private List<TicketPrice> ticketPrices = new ArrayList<TicketPrice>();

	public List<TicketPrice> getTicketPrices() {
		return ticketPrices;
	}

	public void setTicketPrices(List<TicketPrice> ticketPrices) {
		this.ticketPrices = ticketPrices;
	}

}
