package edu.buschartercompany.draftticketprice.entities;

public class DraftTicketPrice {

	private DraftPriceMetadata draftPriceMetadata;
	private Route route;
	private DraftPrice draftPrice;

	public DraftPriceMetadata getDraftPriceMetadata() {
		return draftPriceMetadata;
	}

	public void setDraftPriceMetadata(DraftPriceMetadata draftPriceMetadata) {
		this.draftPriceMetadata = draftPriceMetadata;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public DraftPrice getDraftPrice() {
		return draftPrice;
	}

	public void setDraftPrice(DraftPrice draftPrice) {
		this.draftPrice = draftPrice;
	}

}
