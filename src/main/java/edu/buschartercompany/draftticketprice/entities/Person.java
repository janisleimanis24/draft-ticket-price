package edu.buschartercompany.draftticketprice.entities;

import java.time.LocalDate;

public class Person {

	private String forename;
	private String surname;
	private LocalDate birthDate;
	private PersonCategory category;

	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public PersonCategory getCategory() {
		return category;
	}

	public void setCategory(PersonCategory category) {
		this.category = category;
	}

}
