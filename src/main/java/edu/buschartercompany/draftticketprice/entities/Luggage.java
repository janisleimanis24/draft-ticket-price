package edu.buschartercompany.draftticketprice.entities;

public class Luggage {

	private int cnt;
	/*
	 * in the future it might appear that more just the count of luggage matters, so
	 * it is better to create a separate class for luggage instead of putting
	 * luggage count into Passanger class
	 */

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

}
