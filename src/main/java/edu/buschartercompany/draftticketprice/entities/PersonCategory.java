package edu.buschartercompany.draftticketprice.entities;

public class PersonCategory {

	private String categoryCode;
	private String categoryName;
	private long ageFrom;
	private long ageTo;

	public void set(String categoryCode, String categoryName, long ageFrom, long ageTo) {
		this.categoryCode = categoryCode;
		this.categoryName = categoryName;
		this.ageFrom = ageFrom;
		this.ageTo = ageTo;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public long getAgeFrom() {
		return ageFrom;
	}

	public void setAgeFrom(long ageFrom) {
		this.ageFrom = ageFrom;
	}

	public long getAgeTo() {
		return ageTo;
	}

	public void setAgeTo(long ageTo) {
		this.ageTo = ageTo;
	}

}
