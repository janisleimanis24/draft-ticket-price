package edu.buschartercompany.draftticketprice.entities;

public class Passenger extends Person {

	private Luggage luggage;

	public Luggage getLuggage() {
		return luggage;
	}

	public void setLuggage(Luggage luggage) {
		this.luggage = luggage;
	}

}
