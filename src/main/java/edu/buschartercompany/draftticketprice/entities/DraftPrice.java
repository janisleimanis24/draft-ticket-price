package edu.buschartercompany.draftticketprice.entities;

import java.util.ArrayList;
import java.util.List;

public class DraftPrice {

	private Price totalPrice;
	// private TicketPrices ticketPrices;
	private List<TicketPrice> ticketPrices = new ArrayList<TicketPrice>();

	public Price getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Price totalPrice) {
		this.totalPrice = totalPrice;
	}
	/*
	 * public TicketPrices getTicketPrices() { return ticketPrices; }
	 * 
	 * public void setTicketPrices(TicketPrices ticketPrices) { this.ticketPrices =
	 * ticketPrices; }
	 */

	public List<TicketPrice> getTicketPrices() {
		return ticketPrices;
	}

	public void setTicketPrices(List<TicketPrice> ticketPrices) {
		this.ticketPrices = ticketPrices;
	}

}
