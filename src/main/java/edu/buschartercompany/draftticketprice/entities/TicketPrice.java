package edu.buschartercompany.draftticketprice.entities;

public class TicketPrice {

	private Passenger passenger;
	private Price price;
	private Price luggagePrice;

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public Price getLuggagePrice() {
		return luggagePrice;
	}

	public void setLuggagePrice(Price luggagePrice) {
		this.luggagePrice = luggagePrice;
	}

}
