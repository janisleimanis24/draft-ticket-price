package edu.buschartercompany.draftticketprice.entities;

import java.util.ArrayList;
import java.util.List;

public class DraftPriceRequest {

	private Route route;
	// private Passengers passengers;
	private List<Passenger> passengers = new ArrayList<Passenger>();

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public List<Passenger> getPassengers() {
		return passengers;
	}

	public void setPassengers(List<Passenger> passengers) {
		this.passengers = passengers;
	}

}
