package edu.buschartercompany.draftticketprice.entities;

import java.time.LocalDate;

public class TaxRate {

	private double taxRate;
	private String currencyCode;
	private LocalDate onDate;

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxtRate) {
		this.taxRate = taxtRate;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public LocalDate getOnDate() {
		return onDate;
	}

	public void setOnDate(LocalDate onDate) {
		this.onDate = onDate;
	}

}
