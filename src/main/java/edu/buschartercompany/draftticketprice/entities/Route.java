package edu.buschartercompany.draftticketprice.entities;

public class Route {

	private String routeCode;
	private String routeName;
	private double baseNettoPrice;
	private String currencyCode; // ISO-4217

	public Route() {

	}

	public Route(String routeCode, String routeName, double baseNettoPrice, String currencyCode) {
		this.routeCode = routeCode;
		this.routeName = routeName;
		this.baseNettoPrice = baseNettoPrice;
		this.currencyCode = currencyCode;
	}

	public String getRouteCode() {
		return routeCode;
	}

	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public double getBaseNettoPrice() {
		return baseNettoPrice;
	}

	public void setBaseNettoPrice(double baseNettoPrice) {
		this.baseNettoPrice = baseNettoPrice;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

}
