package edu.buschartercompany.draftticketprice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DraftTicketPriceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DraftTicketPriceApplication.class, args);
	}

}
