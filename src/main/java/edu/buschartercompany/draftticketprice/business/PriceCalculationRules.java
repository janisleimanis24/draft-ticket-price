package edu.buschartercompany.draftticketprice.business;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import edu.buschartercompany.draftticketprice.Util;
import edu.buschartercompany.draftticketprice.entities.PersonCategory;
import edu.buschartercompany.draftticketprice.entities.Price;
import edu.buschartercompany.draftticketprice.exceptions.IllegalAgeException;
import edu.buschartercompany.draftticketprice.exceptions.RouteInputException;
import edu.buschartercompany.draftticketprice.services.RouteBasePriceService;
import edu.buschartercompany.draftticketprice.services.TaxRateService;
import edu.buschartercompany.draftticketprice.simulators.PersonCategoryDBSimulator;

public class PriceCalculationRules {

	private final int LUGGAGE_PRICE = 30; // % of base price
	private final int CHILD_PRICE = 50; // % of base price
	private LocalDate today = LocalDate.now();

	public String getCategoryCodeByBirthDate(LocalDate birthDate) throws IllegalAgeException {
		long age = ChronoUnit.YEARS.between(birthDate, this.today);
		PersonCategoryDBSimulator pc = new PersonCategoryDBSimulator();
		PersonCategory category = pc.searchByAge(age);
		return category.getCategoryCode();
	}

	public double getLuggagePrice(String routeCode) throws RouteInputException {
		Util.fmt(routeCode);
		RouteBasePriceService rbps = new RouteBasePriceService(routeCode);
		double luggPrice = rbps.getRoute().getBaseNettoPrice() * LUGGAGE_PRICE / 100; // 30% or the base price
		return luggPrice;
	}

	public double getPersonsPrice(String routeCode, String category) throws RouteInputException {
		Util.fmt(category);
		RouteBasePriceService rbps = new RouteBasePriceService(routeCode);
		double personsPrice;
		switch (category) {
		case "CHILD":
			personsPrice = rbps.getRoute().getBaseNettoPrice()
					- (rbps.getRoute().getBaseNettoPrice() * CHILD_PRICE / 100);
			break;
		// in the future alter this part, when more business rules on discounts appear
		default:
			personsPrice = rbps.getRoute().getBaseNettoPrice();
			break;
		}
		return personsPrice;
	}

	public Price getPriceWithTaxes(double nettoPrice) {
		Price p = new Price();
		p.setPriceNetto(nettoPrice);
		TaxRateService trs = new TaxRateService();
		p.setTaxRate(trs.getTaxRate().getTaxRate());
		p.setCurrencyCode(trs.getTaxRate().getCurrencyCode());
		p.setPriceBrutto(nettoPrice + (nettoPrice * trs.getTaxRate().getTaxRate() / 100));
		return p;
	}

}
