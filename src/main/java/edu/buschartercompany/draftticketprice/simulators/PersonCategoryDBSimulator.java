package edu.buschartercompany.draftticketprice.simulators;

import java.util.ArrayList;
import java.util.List;

import edu.buschartercompany.draftticketprice.entities.PersonCategory;
import edu.buschartercompany.draftticketprice.exceptions.IllegalAgeException;
import edu.buschartercompany.draftticketprice.exceptions.PersonCategoryInputException;

public class PersonCategoryDBSimulator {

	private List<PersonCategory> categories = new ArrayList<PersonCategory>();

	public PersonCategoryDBSimulator() {
		PersonCategory pc = new PersonCategory();
		pc.set("ADULT", "Adult person", 18, 150);
		this.categories.add(pc);
		pc = new PersonCategory();
		pc.set("CHILD", "Child", 0, 17);
		this.categories.add(pc);
		pc = new PersonCategory();
	}

	public PersonCategory searchByCategoryCode(String categoryCode) throws PersonCategoryInputException {
		for (PersonCategory c : categories) {
			if (c.getCategoryCode().equals(categoryCode)) {
				return c;
			}
		}
		throw new PersonCategoryInputException("Invalid person category code.");
	}

	public PersonCategory searchByAge(long age) throws IllegalAgeException {
		for (int i = 0; i < this.categories.size(); i++) {
			if (this.categories.get(i).getAgeFrom() <= age && this.categories.get(i).getAgeTo() >= age) {
				return this.categories.get(i);
				// assuming the configuration itself is correct and there's only one entry that
				// satisfies the conditions
			}
		}
		throw new IllegalAgeException("Person category is not defined for the specified age of: " + age);
	}

}
