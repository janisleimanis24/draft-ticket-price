package edu.buschartercompany.draftticketprice.simulators;

import java.time.LocalDate;

import edu.buschartercompany.draftticketprice.entities.TaxRate;

public class TaxRateSimulator {

	private static final double TAX_RATE = 21;
	private static final String CURRENCY_CODE = "EUR";

	public TaxRate getTaxRate(LocalDate onDate) {
		TaxRate tr = new TaxRate();
		tr.setTaxRate(TAX_RATE);
		tr.setCurrencyCode(CURRENCY_CODE);
		tr.setOnDate(onDate);
		return tr;
	}

}
