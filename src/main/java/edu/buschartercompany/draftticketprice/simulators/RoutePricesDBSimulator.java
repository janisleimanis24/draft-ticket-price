package edu.buschartercompany.draftticketprice.simulators;

import java.util.ArrayList;
import java.util.List;

import edu.buschartercompany.draftticketprice.entities.Route;
import edu.buschartercompany.draftticketprice.exceptions.RouteInputException;

public class RoutePricesDBSimulator {

	private List<Route> routes = new ArrayList<Route>();

	public RoutePricesDBSimulator() {
		Route r = new Route("LT_VILNIUS", "Vilnius, Lithuania", 10d, "EUR");
		this.routes.add(r);
		r = new Route("EE_VALGA", "Valga, Estonia", 5d, "EUR");
		this.routes.add(r);
	}

	public Route searchByRouteCode(String routeCode) throws RouteInputException {
		for (Route r : routes) {
			if (r.getRouteCode().equals(routeCode)) {
				return r;
			}
		}
		throw new RouteInputException("Invalid route code.");
	}

	public List<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}

}
