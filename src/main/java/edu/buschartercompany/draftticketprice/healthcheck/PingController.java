package edu.buschartercompany.draftticketprice.healthcheck;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Healthcheck")
public class PingController {

	@RequestMapping(value = { "/Ping" }, produces = { "application/json" }, method = RequestMethod.GET)
	public @ResponseBody Ping get() {
		return new Ping();
	}

}
