package edu.buschartercompany.draftticketprice.healthcheck;

public class Ping {

	private boolean serviceAvailable = false;

	public Ping() {
		// Some logic to detect service is up and running
		// ...
		this.serviceAvailable = true;
	}

	public boolean isServiceAvailable() {
		return serviceAvailable;
	}

	public void setServiceAvailable(boolean serviceAvailable) {
		this.serviceAvailable = serviceAvailable;
	}

}
