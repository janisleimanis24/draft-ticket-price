package edu.buschartercompany.draftticketprice.contollers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.buschartercompany.draftticketprice.services.TaxRateService;

@RestController
@RequestMapping(value = { "/TaxRate" }, produces = { "application/json" })
public class TaxRateController {

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<TaxRateService> get() {
		// return new ResponseEntity<>(carData.get(), HttpStatus.OK);
		TaxRateService s = new TaxRateService();
		return new ResponseEntity<>(s, HttpStatus.OK);
	}

}
