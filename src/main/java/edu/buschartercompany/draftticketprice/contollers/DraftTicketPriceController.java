package edu.buschartercompany.draftticketprice.contollers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.buschartercompany.draftticketprice.entities.DraftPriceRequest;
import edu.buschartercompany.draftticketprice.exceptions.ErrorInfo;
import edu.buschartercompany.draftticketprice.exceptions.IllegalAgeException;
import edu.buschartercompany.draftticketprice.exceptions.PersonCategoryInputException;
import edu.buschartercompany.draftticketprice.exceptions.RouteInputException;
import edu.buschartercompany.draftticketprice.services.DraftTicketPriceService;

@RestController
@RequestMapping(value = { "/DraftTicketPrice" }, produces = { "application/json" })
public class DraftTicketPriceController {

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody DraftTicketPriceService get(@RequestBody(required = true) DraftPriceRequest request)
			throws RouteInputException, IllegalAgeException, PersonCategoryInputException {
		DraftTicketPriceService dtps = new DraftTicketPriceService();
		dtps.DraftTicketPrice(request);
		return dtps;
	}

	@ExceptionHandler(RouteInputException.class)
	public @ResponseBody ResponseEntity<ErrorInfo> handleException(RouteInputException e) {
		return new ResponseEntity<>(new ErrorInfo("ROUTE_INPUT_EXCEPTION", e.getMessage()), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(IllegalAgeException.class)
	public @ResponseBody ResponseEntity<ErrorInfo> handleException(IllegalAgeException e) {
		return new ResponseEntity<>(new ErrorInfo("ILLEGAL_AGE_EXCEPTION", e.getMessage()), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(PersonCategoryInputException.class)
	public @ResponseBody ResponseEntity<ErrorInfo> handleException(PersonCategoryInputException e) {
		return new ResponseEntity<>(new ErrorInfo("PERSON_CATEGORY_EXCEPTION", e.getMessage()), HttpStatus.BAD_REQUEST);
	}

}
