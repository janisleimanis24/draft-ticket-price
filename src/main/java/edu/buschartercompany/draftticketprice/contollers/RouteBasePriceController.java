package edu.buschartercompany.draftticketprice.contollers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.buschartercompany.draftticketprice.exceptions.ErrorInfo;
import edu.buschartercompany.draftticketprice.exceptions.RouteInputException;
import edu.buschartercompany.draftticketprice.services.RouteBasePriceService;

@RestController
@RequestMapping(value = { "/RouteBasePrice" }, produces = { "application/json" })
public class RouteBasePriceController {

	@RequestMapping(method = RequestMethod.GET, value = "/{routeCode}")
	public @ResponseBody ResponseEntity<RouteBasePriceService> get(@PathVariable String routeCode)
			throws RouteInputException {
		RouteBasePriceService s;
		// try {
		s = new RouteBasePriceService(routeCode);
		return new ResponseEntity<>(s, HttpStatus.OK);
		// } catch (RouteInputException e) {
		// return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		// }
	}

	@ExceptionHandler(RouteInputException.class)
	public @ResponseBody ResponseEntity<ErrorInfo> handleException(RouteInputException e) {
		return new ResponseEntity<>(new ErrorInfo("ROUTE_INPUT_EXCEPTION", e.getMessage()), HttpStatus.BAD_REQUEST);
	}

}
