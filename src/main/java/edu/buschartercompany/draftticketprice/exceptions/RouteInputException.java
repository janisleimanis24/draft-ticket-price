package edu.buschartercompany.draftticketprice.exceptions;

public class RouteInputException extends Exception {

	private static final long serialVersionUID = 1L;

	public RouteInputException(String msg) {
		super(msg);
	}

}
