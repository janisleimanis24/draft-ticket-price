package edu.buschartercompany.draftticketprice.exceptions;

public class PersonCategoryInputException extends Exception {

	private static final long serialVersionUID = 1L;

	public PersonCategoryInputException(String msg) {
		super(msg);
	}
}
