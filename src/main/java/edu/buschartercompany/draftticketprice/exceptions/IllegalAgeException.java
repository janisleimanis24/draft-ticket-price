package edu.buschartercompany.draftticketprice.exceptions;

public class IllegalAgeException extends Exception {

	private static final long serialVersionUID = 1L;

	public IllegalAgeException(String msg) {
		super(msg);
	}

}
