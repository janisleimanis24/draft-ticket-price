package edu.buschartercompany.draftticketprice.services;

import java.time.LocalDate;

import edu.buschartercompany.draftticketprice.entities.TaxRate;
import edu.buschartercompany.draftticketprice.simulators.TaxRateSimulator;

public class TaxRateService {

	private TaxRate taxRate;

	public TaxRateService() {
		// getting data from external service (simulating)
		this.taxRate = new TaxRateSimulator().getTaxRate(LocalDate.now());
	}

	public TaxRate getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(TaxRate taxRate) {
		this.taxRate = taxRate;
	}

}
