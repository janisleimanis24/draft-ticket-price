package edu.buschartercompany.draftticketprice.services;

import edu.buschartercompany.draftticketprice.Util;
import edu.buschartercompany.draftticketprice.entities.Route;
import edu.buschartercompany.draftticketprice.exceptions.RouteInputException;
import edu.buschartercompany.draftticketprice.simulators.RoutePricesDBSimulator;

public class RouteBasePriceService {

	private Route route;

	public RouteBasePriceService(String routeCode) throws RouteInputException {
		Util.fmt(routeCode);
		Route route = new RoutePricesDBSimulator().searchByRouteCode(routeCode);
		this.route = route;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

}
