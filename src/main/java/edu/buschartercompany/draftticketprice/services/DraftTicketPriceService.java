package edu.buschartercompany.draftticketprice.services;

import java.util.ArrayList;
import java.util.List;

import edu.buschartercompany.draftticketprice.Util;
import edu.buschartercompany.draftticketprice.business.PriceCalculationRules;
import edu.buschartercompany.draftticketprice.entities.DraftPrice;
import edu.buschartercompany.draftticketprice.entities.DraftPriceMetadata;
import edu.buschartercompany.draftticketprice.entities.DraftPriceRequest;
import edu.buschartercompany.draftticketprice.entities.DraftTicketPrice;
import edu.buschartercompany.draftticketprice.entities.Luggage;
import edu.buschartercompany.draftticketprice.entities.Passenger;
import edu.buschartercompany.draftticketprice.entities.PersonCategory;
import edu.buschartercompany.draftticketprice.entities.Price;
import edu.buschartercompany.draftticketprice.entities.TicketPrice;
import edu.buschartercompany.draftticketprice.exceptions.IllegalAgeException;
import edu.buschartercompany.draftticketprice.exceptions.PersonCategoryInputException;
import edu.buschartercompany.draftticketprice.exceptions.RouteInputException;
import edu.buschartercompany.draftticketprice.simulators.PersonCategoryDBSimulator;

public class DraftTicketPriceService {

	private DraftTicketPrice draftTicketPrice;

	public void DraftTicketPrice(DraftPriceRequest draftPriceRequest)
			throws RouteInputException, IllegalAgeException, PersonCategoryInputException {
		this.draftTicketPrice = new DraftTicketPrice();
		this.draftTicketPrice.setDraftPriceMetadata(new DraftPriceMetadata());
		String inputRouteCode = Util.fmt(draftPriceRequest.getRoute().getRouteCode());
		RouteBasePriceService rbps = new RouteBasePriceService(inputRouteCode);
		DraftPrice draftPrice = new DraftPrice();
		List<TicketPrice> ticketPricesList = new ArrayList<TicketPrice>();
		Price totalPrice = new Price();
		this.draftTicketPrice.setRoute(rbps.getRoute());
		for (int i = 0; i < draftPriceRequest.getPassengers().size(); i++) {
			PriceCalculationRules cr = new PriceCalculationRules();
			String categoryCode = cr
					.getCategoryCodeByBirthDate(draftPriceRequest.getPassengers().get(i).getBirthDate());
			double personPriceNetto = cr.getPersonsPrice(inputRouteCode, categoryCode);
			double luggagePriceNetto = cr.getLuggagePrice(inputRouteCode);
			Price persPrice = cr.getPriceWithTaxes(personPriceNetto);
			Passenger passenger = new Passenger();
			passenger.setForename(draftPriceRequest.getPassengers().get(i).getForename());
			passenger.setSurname(draftPriceRequest.getPassengers().get(i).getSurname());
			passenger.setBirthDate(draftPriceRequest.getPassengers().get(i).getBirthDate());
			PersonCategoryDBSimulator pcData = new PersonCategoryDBSimulator();
			// getting category info
			PersonCategory pc = pcData.searchByCategoryCode(categoryCode);
			passenger.setCategory(pc);
			Luggage luggage = new Luggage();
			luggage.setCnt(draftPriceRequest.getPassengers().get(i).getLuggage().getCnt());
			Price luggPrice = cr.getPriceWithTaxes(
					luggagePriceNetto * draftPriceRequest.getPassengers().get(i).getLuggage().getCnt());
			passenger.setLuggage(luggage);
			TicketPrice ticketPrice = new TicketPrice();
			ticketPrice.setPassenger(passenger);
			ticketPrice.setPrice(persPrice);
			ticketPrice.setLuggagePrice(luggPrice);
			draftPrice.setTicketPrices(null);
			ticketPricesList.add(ticketPrice);
			if (i == 0) { // tax rate & currency is not changing over one list, so we can take the first
							// one
				totalPrice.setTaxRate(persPrice.getTaxRate());
				totalPrice.setCurrencyCode(persPrice.getCurrencyCode());
			}
			totalPrice
					.setPriceNetto(totalPrice.getPriceNetto() + persPrice.getPriceNetto() + luggPrice.getPriceNetto());
			totalPrice.setPriceBrutto(
					totalPrice.getPriceBrutto() + persPrice.getPriceBrutto() + luggPrice.getPriceBrutto());
		}
		draftPrice.setTicketPrices(ticketPricesList);
		draftPrice.setTotalPrice(totalPrice);
		this.draftTicketPrice.setDraftPrice(draftPrice);
	}

	public DraftTicketPrice getDraftTicketPrice() {
		return draftTicketPrice;
	}

	public void setDraftTicketPrice(DraftTicketPrice draftTicketPrice) {
		this.draftTicketPrice = draftTicketPrice;
	}

}
